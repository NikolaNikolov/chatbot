﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace ChatBotProgramOClient.Behavior
{
    public static class EnterKeyDownCommandBehavior
    {
        public static ICommand GetCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command", typeof(ICommand), typeof(EnterKeyDownCommandBehavior),
                new PropertyMetadata(null, OnCommandChanged));

        public static object GetCommandParameter(DependencyObject obj)
        {
            return obj.GetValue(CommandParameterProperty);
        }

        public static void SetCommandParameter(DependencyObject obj, object value)
        {
            obj.SetValue(CommandParameterProperty, value);
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.RegisterAttached("CommandParameter", typeof(object), typeof(EnterKeyDownCommandBehavior),
                new PropertyMetadata(null, OnCommandParameterChanged));

        private static bool GetHasCommandParameter(DependencyObject obj)
        {
            return (bool)obj.GetValue(hasCommandParameterProperty);
        }

        private static void SetHasCommandParameter(DependencyObject obj, bool value)
        {
            obj.SetValue(hasCommandParameterProperty, value);
        }

        private static readonly DependencyProperty hasCommandParameterProperty =
            DependencyProperty.RegisterAttached("HasCommandParameter", typeof(bool), typeof(EnterKeyDownCommandBehavior),
                new PropertyMetadata(false));

        private static void OnCommandParameterChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            SetHasCommandParameter(o, true);
        }

        private static void OnCommandChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement element = o as FrameworkElement;
            if (element != null)
            {
                if (e.NewValue == null)
                {
                    element.KeyDown -= new KeyEventHandler(FrameworkElement_KeyDown);
                }
                else if (e.OldValue == null)
                {
                    element.KeyDown += new KeyEventHandler(FrameworkElement_KeyDown);
                }
            }
        }

        private static void FrameworkElement_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == VirtualKey.Enter)
            {
                DependencyObject o = sender as DependencyObject;
                ICommand command = GetCommand(sender as DependencyObject);

                FrameworkElement element = e.OriginalSource as FrameworkElement;
                if (element != null)
                {
                    if (GetHasCommandParameter(o))
                    {
                        object commandParameter = GetCommandParameter(o);

                        if (command.CanExecute(commandParameter))
                        {
                            command.Execute(commandParameter);
                        }
                    }
                    else if (command.CanExecute(element.DataContext))
                    {
                        command.Execute(element.DataContext);
                    }
                }
            }
        }
    }
}
