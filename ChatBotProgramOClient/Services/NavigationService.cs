﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ChatBotProgramOClient.Services
{
    public class NavigationService : INavigationService
    {
        private static NavigationService instance;
        private const string ViewsNamespace = "ChatBotProgramOClient.Views.";
        private static Frame frame;

        public static INavigationService Current
        {
            get
            {
                if (instance == null)
                {
                    instance = new NavigationService();
                }

                return instance;
            }
        }

        private static Frame Frame
        {
            get
            {
                if (frame == null)
                {
                    frame = (Frame)Window.Current.Content;
                }

                return frame;
            }
        }

        public void GoHome()
        {
            if (Frame != null)
            {
                while (Frame.CanGoBack)
                {
                    Frame.GoBack();
                }
            }
        }

        public void GoBack()
        {
            if (Frame != null && Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }

        public void GoForward()
        {
            if (Frame != null && Frame.CanGoForward)
            {
                Frame.GoForward();
            }
        }

        public void Navigate(string pageName)
        {
            Navigate(pageName, null);
        }

        public void Navigate(string pageName, object parameter)
        {
            if (Frame != null)
            {
                var pageType = Type.GetType(ViewsNamespace + pageName);
                Frame.Navigate(pageType, parameter);
            }
        }

        public bool CanGoBack
        {
            get
            {
                if (Frame != null)
                {
                    return Frame.CanGoBack;
                }

                return false;
            }
        }

        public bool CanGoForward
        {
            get
            {
                if (Frame != null)
                {
                    return Frame.CanGoForward;
                }

                return false;
            }
        }
    }
}
