﻿using ChatBotProgramOClient.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Services
{
    public static class BotServices
    {
        private const string BotServicesUrl = "http://api.program-o.com/v2.3.1/chatbot/";

        internal static async Task<ChatDialog> GetBotAnswer(int botId, string conversationId, string userMessage)
        {
            string encodedMessage = WebUtility.UrlEncode(userMessage);
            HttpClient client = new HttpClient();
            var requestUrl = BotServicesUrl + "?say=" + encodedMessage + "&bot_id=" + botId + "&convo_id=" + conversationId;
            var response = await client.GetAsync(requestUrl);
            var content = await response.Content.ReadAsStringAsync();
            var chatDialog = await JsonConvert.DeserializeObjectAsync<ChatDialog>(content);
            return chatDialog;
        }
    }
}
