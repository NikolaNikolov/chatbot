﻿using ChatBotProgramOClient.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Models
{
    public class MainMenuItem
    {
        public string ImageUrl { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public BotType BotType { get; set; }
    }
}
