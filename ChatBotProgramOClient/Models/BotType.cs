﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Models
{
    public enum BotType
    {
        ProgramO = 6,
        ShakespeareBot = 10,
        CarlosChow = 13
    }
}
