﻿using ChatBotProgramOClient.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Models
{
    public class ChatMessage
    {
        public string Message { get; set; }

        public MessageOwner Owner { get; set; }
    }
}
