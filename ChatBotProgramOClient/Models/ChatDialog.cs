﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Models
{
    [DataContract]
    public class ChatDialog
    {
        [DataMember(Name = "convo_id")]
        public string ConversationId { get; set; }

        [DataMember(Name = "usersay")]
        public string UserMessage { get; set; }

        [DataMember(Name = "botsay")]
        public string BotMessage { get; set; }
    }
}
