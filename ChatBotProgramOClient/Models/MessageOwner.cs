﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatBotProgramOClient.Models
{
    public enum MessageOwner
    {
        User = 0,
        Bot = 1
    }
}
