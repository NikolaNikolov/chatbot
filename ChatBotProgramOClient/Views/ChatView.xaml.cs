﻿using ChatBotProgramOClient.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace ChatBotProgramOClient.Views
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ChatView : ViewBase
    {
        private PrintServiceProvider printServiceProvider;

        public ChatView()
        {
            this.InitializeComponent();
        }

        public void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                ChatInput.Focus(FocusState.Programmatic);
                var selectedIndex = MessagesListView.Items.Count - 1;
                if (selectedIndex >= 0)
                {
                    MessagesListView.SelectedIndex = selectedIndex;

                    MessagesListView.ScrollIntoView(MessagesListView.SelectedItem);
                }
            });
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.printServiceProvider = new PrintServiceProvider();
            this.printServiceProvider.RegisterForPrinting(this, typeof(PrintChatView), this.DataContext);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.printServiceProvider.UnregisterForPrinting();
        }
    }
}
