﻿namespace ChatBotProgramOClient.Infrastructure
{
    using System;

    public class PrintServiceEventArgs : EventArgs
    {
        public PrintServiceEventArgs()
        { }

        public PrintServiceEventArgs(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// The message from the print service.
        /// </summary>
        public string Message { get; set; }
    }
}
