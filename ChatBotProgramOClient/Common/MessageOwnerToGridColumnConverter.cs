﻿using ChatBotProgramOClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace ChatBotProgramOClient.Common
{
    public class MessageOwnerToGridColumnConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (targetType != typeof(int))
            {
                return null;
            }

            var messageOwner = (MessageOwner)value;
            return (int)messageOwner;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
