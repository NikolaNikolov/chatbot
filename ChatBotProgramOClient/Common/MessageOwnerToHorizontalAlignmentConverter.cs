﻿using ChatBotProgramOClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ChatBotProgramOClient.Common
{
    public class MessageOwnerToHorizontalAlignmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (targetType != typeof(HorizontalAlignment))
            {
                return null;
            }
            
            var messageOwner = (MessageOwner)value;
            switch (messageOwner)
            {
                case MessageOwner.User:
                    return HorizontalAlignment.Left;
                case MessageOwner.Bot:
                    return HorizontalAlignment.Right;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
