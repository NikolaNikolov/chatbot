﻿using ChatBotProgramOClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace ChatBotProgramOClient.Common
{
    public class MessageOwnerToForegroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (targetType != typeof(Brush))
            {
                return null;
            }

            var messageOwner = (MessageOwner)value;
            switch (messageOwner)
            {
                case MessageOwner.User:
                    return new SolidColorBrush(Colors.Aquamarine);
                case MessageOwner.Bot:
                    return new SolidColorBrush(Colors.Khaki);
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
