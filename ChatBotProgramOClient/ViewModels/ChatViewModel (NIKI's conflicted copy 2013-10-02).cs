﻿using ChatBotProgramOClient.Behavior;
using ChatBotProgramOClient.Models;
using ChatBotProgramOClient.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Notifications;
using Windows.UI.Xaml.Controls;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.Storage.Streams;
using System.IO;
using Windows.Storage.Pickers;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Data.Xml.Dom;
using Windows.UI.Xaml;

namespace ChatBotProgramOClient.ViewModels
{
    public class ChatViewModel : ViewModelBase
    {
        private string chatInput;
        private int botId;
        private string conversationId;
        private const string ChatMessagesToken = "ChatMessagesToken";
        private const string ChatInputToken = "ChatInputToken";
        private const string BotIdToken = "BotIdToken";
        private const string ConversationIdToken = "ConversationidToken";
        private readonly List<string> plainTextFileTypes = new List<string>() { ".txt" };
        private readonly IPropertySet localSettingsValues = ApplicationData.Current.LocalSettings.Values;
        private DataTransferManager dataTransferManager;

        public ChatViewModel()
        {
            this.SendMessage = new RelayCommand(this.OnSendMessage);
            this.AppBarHome = new RelayCommand(this.OnAppBarHome);
            this.AppBarClear = new RelayCommand(this.OnAppBarClear);
            this.AppBarExport = new RelayCommand(this.OnAppBarExport);
            this.AppBarImport = new RelayCommand(this.OnAppBarImport);
            this.AppBarShare = new RelayCommand(this.OnAppBarShare);
        }

        public string ChatInput
        {
            get
            {
                return chatInput;
            }
            set
            {
                SetProperty(ref chatInput, value);
            }
        }

        public ObservableCollection<ChatMessage> ChatMessages { get; set; }

        public RelayCommand SendMessage { get; private set; }

        public RelayCommand AppBarHome { get; private set; }

        public RelayCommand AppBarClear { get; private set; }

        public RelayCommand AppBarExport { get; private set; }

        public RelayCommand AppBarImport { get; private set; }

        public RelayCommand AppBarShare { get; private set; }

        private async void OnSendMessage(object parameter)
        {
            if (!string.IsNullOrEmpty(ChatInput))
            {
                var chatMessage = new ChatMessage()
                {
                    Message = ChatInput,
                    Owner = MessageOwner.User
                };
                ChatInput = string.Empty;

                this.ChatMessages.Add(chatMessage);
                var messagesListView = parameter as ListView;
                ScrollListViewToBottom(messagesListView);

                bool hasConnectionProblems = false;
                try
                {
                    var responseChatDialog = await BotServices.GetBotAnswer(this.botId, this.conversationId, chatMessage.Message);
                    ChatMessage responseChatMessage = new ChatMessage()
                    {
                        Message = responseChatDialog.BotMessage,
                        Owner = MessageOwner.Bot
                    };
                    this.ChatMessages.Add(responseChatMessage);
                    ScrollListViewToBottom(messagesListView);
                    localSettingsValues[ConversationIdToken + this.conversationId] = JsonConvert.SerializeObject(this.ChatMessages);
                }
                catch (Exception ex)
                {
                    hasConnectionProblems = true;
                }

                if (hasConnectionProblems)
                {
                    var messageDialog = new MessageDialog("Please check your internet connection.", "Something went wrong.");
                    messageDialog.Commands.Add(new UICommand("Ok"));
                    await messageDialog.ShowAsync();
                }
            }
        }

        private void OnAppBarHome(object parameter)
        {
            NavigationService.GoHome();
        }

        private async void OnAppBarClear(object parameter)
        {
            var messageDialog = new MessageDialog("You are about to permanently delete the current bot chat history.", "Are you sure?");
            messageDialog.Commands.Add(new UICommand(
                "Ok",
                new UICommandInvokedHandler(this.OnAppBarClearConfirmation)));
            messageDialog.Commands.Add(new UICommand("Cancel"));
            messageDialog.DefaultCommandIndex = 0;
            messageDialog.CancelCommandIndex = 1;
            await messageDialog.ShowAsync();
        }

        private void OnAppBarClearConfirmation(IUICommand command)
        {
            this.ChatMessages.Clear();
            this.localSettingsValues.Remove(ConversationIdToken + this.conversationId);
            this.localSettingsValues.Remove(BotIdToken + this.botId);
            RaiseToastNotification("Chat history cleared successfully!");
        }

        private async void OnAppBarExport(object parameter)
        {
            var savePicker = new FileSavePicker();
            savePicker.FileTypeChoices.Add("Plain Text", plainTextFileTypes);
            savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            savePicker.SuggestedFileName = "ChatBot History.txt";
            var saveFile = await savePicker.PickSaveFileAsync();
            if (saveFile != null)
            {
                string textContent = await this.GenerateTextFromHistoryAsync();
                await Windows.Storage.FileIO.WriteTextAsync(saveFile, textContent);
                RaiseToastNotification("Chat history saved successfully!");
            }
        }

        public string TextForPrinting
        {
            get
            {
                return GenerateTextFromHistoryAsync().Result;
            }
        }

        public string ChatTitle
        {
            get
            {
                return "Chat with " + (BotType)this.botId;
            }
        }

        private double chatWidth = 0;
        private double chatHeight = 0;

        public double ChatWidth
        {
            get
            {
                if (chatWidth == 0)
                {
                    chatWidth = Window.Current.Bounds.Width - 480;
                }

                return chatWidth;
            }
        }

        public double ChatHeight
        {
            get
            {
                if (chatHeight == 0)
                {
                    chatHeight = Window.Current.Bounds.Height - 340;
                }

                return chatHeight;
            }
        }

        private Task<string> GenerateTextFromHistoryAsync()
        {
            return Task.Run(() =>
            {
                StringBuilder textBuilder = new StringBuilder();
                textBuilder.AppendFormat("ChatBot Dialog History {0}", this.conversationId);
                textBuilder.AppendLine();
                textBuilder.AppendFormat("Generated at {0}", DateTime.Now);
                textBuilder.AppendLine(Environment.NewLine);

                foreach (var chatMessage in this.ChatMessages)
                {
                    switch (chatMessage.Owner)
                    {
                        case MessageOwner.User:
                            textBuilder.Append("You: ");
                            break;
                        case MessageOwner.Bot:
                            textBuilder.Append("Bot: ");
                            break;
                    }

                    textBuilder.AppendLine(chatMessage.Message);
                }

                return textBuilder.ToString();
            });
        }

        private async void OnAppBarImport(object parameter)
        {
            var messageDialog = new MessageDialog("If you select a file you will permanently replace the current bot chat history.");
            messageDialog.Commands.Add(new UICommand("Ok"));
            messageDialog.DefaultCommandIndex = 0;
            messageDialog.CancelCommandIndex = 0;
            await messageDialog.ShowAsync();

            var openPicker = new FileOpenPicker();
            openPicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            openPicker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
            foreach (var fileType in plainTextFileTypes)
            {
                openPicker.FileTypeFilter.Add(fileType);
            }

            var file = await openPicker.PickSingleFileAsync();
            if (file != null)
            {
                GenerateHistoryFromFileAsync(file);
            }
        }

        private async void GenerateHistoryFromFileAsync(StorageFile file)
        {
            using (var winRtStream = await file.OpenAsync(FileAccessMode.Read))
            {
                Stream dotnetStream = winRtStream.AsStreamForRead();
                using (var streamReader = new StreamReader(dotnetStream))
                {
                    bool isCorruptedFile = false;
                    try
                    {
                        this.ChatMessages.Clear();
                        this.localSettingsValues.Remove(ConversationIdToken + this.conversationId);
                        string line = streamReader.ReadLine();
                        int conversationIdIndex = line.LastIndexOf(' ') + 1;
                        this.conversationId = line.Substring(conversationIdIndex);
                        line = streamReader.ReadLine();
                        line = streamReader.ReadLine();
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            ChatMessage chatMessage = new ChatMessage();
                            string messageSender = line.Substring(0, 3);
                            switch (messageSender)
                            {
                                case "You":
                                    chatMessage.Owner = MessageOwner.User;
                                    break;
                                case "Bot":
                                    chatMessage.Owner = MessageOwner.Bot;
                                    break;
                            }

                            chatMessage.Message = line.Substring(5);
                            this.ChatMessages.Add(chatMessage);
                        }

                        this.localSettingsValues[BotIdToken + this.botId] = this.conversationId;
                        this.localSettingsValues[ConversationIdToken + this.conversationId] = JsonConvert.SerializeObject(this.ChatMessages);
                        RaiseToastNotification("Chat history loaded successfully!");
                    }
                    catch (Exception ex)
                    {
                        isCorruptedFile = true;
                        this.localSettingsValues.Remove(BotIdToken + this.botId);
                        this.conversationId = DateTime.Now.Ticks.ToString();
                    }

                    if (isCorruptedFile)
                    {
                        var messageDialog = new MessageDialog("Your file is either not a history file or is corrupted.", "Something went wrong.");
                        messageDialog.Commands.Add(new UICommand("Ok"));
                        messageDialog.DefaultCommandIndex = 0;
                        messageDialog.CancelCommandIndex = 0;
                        await messageDialog.ShowAsync();
                    }
                }
            }
        }

        private void RaiseToastNotification(
            string text,
            bool isSilent = true,
            string imagePath = "ms-appx:///Assets/programo_avatar.png")
        {
            string toastXmlString =
                "<toast>" +
                "<visual version='1'>" +
                "<binding template='toastImageAndText01'>" +
                "<text id='1'>" + text + "</text>" +
                "<image id='1' src='" + imagePath + "' alt='BotChat Logo'/>" +
                "</binding>" +
                "</visual>" +
                "<audio silent='" + isSilent.ToString().ToLower() + "'/>" +
                "</toast>";
            XmlDocument toastDom = new XmlDocument();
            toastDom.LoadXml(toastXmlString);
            ToastNotification toast = new ToastNotification(toastDom);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void OnAppBarShare(object parameter)
        {
            DataTransferManager.ShowShareUI();
        }

        private void ScrollListViewToBottom(ListView messagesListView)
        {
            var selectedIndex = messagesListView.Items.Count - 1;
            if (selectedIndex >= 0)
            {
                messagesListView.SelectedIndex = selectedIndex;
                messagesListView.UpdateLayout();

                messagesListView.ScrollIntoView(messagesListView.SelectedItem);
            }
        }

        public override void LoadState(object navParameter, Dictionary<string, object> viewModelState)
        {
            this.dataTransferManager = DataTransferManager.GetForCurrentView();
            this.dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
            botId = (int)navParameter;

            if (viewModelState != null)
            {
                this.ChatMessages = (ObservableCollection<ChatMessage>)viewModelState[ChatMessagesToken];
                this.ChatInput = (string)viewModelState[ChatInputToken];
            }
            else
            {
                if (localSettingsValues.ContainsKey(BotIdToken + this.botId))
                {
                    this.conversationId = (string)localSettingsValues[BotIdToken + this.botId];
                    try
                    {
                        this.ChatMessages = JsonConvert.DeserializeObject<ObservableCollection<ChatMessage>>(
                            (string)localSettingsValues[ConversationIdToken + this.conversationId]);
                    }
                    catch (Exception ex)
                    {
                        InitializeChat();
                    }
                }
                else
                {
                    InitializeChat();
                }
            }
        }

        private void InitializeChat()
        {
            this.ChatMessages = new ObservableCollection<ChatMessage>();
            this.conversationId = DateTime.Now.Ticks.ToString();
            this.localSettingsValues[BotIdToken + this.botId] = this.conversationId;
            this.localSettingsValues[ConversationIdToken + this.conversationId] = JsonConvert.SerializeObject(this.ChatMessages);
        }

        public override void SaveState(Dictionary<string, object> viewModelState)
        {
            this.dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
            if (this.localSettingsValues.ContainsKey(ConversationIdToken + this.conversationId))
            {
                viewModelState[ChatMessagesToken] = this.ChatMessages;
                viewModelState[ChatInputToken] = this.ChatInput;
            }
        }

        private async void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            DataRequest request = args.Request;
            request.Data.Properties.ApplicationName = "ChatBot";
            request.Data.Properties.Title = string.Format("ChatBot {0} History", (BotType)this.botId);
            request.Data.Properties.Description = "ChatBot is a simple AI chat application developed just as a fun project.";
            string textToShare = await this.GenerateTextFromHistoryAsync();
            string textWithBrTags = textToShare.Replace(Environment.NewLine, "<br />");
            string textAsHtml = HtmlFormatHelper.CreateHtmlFormat(textWithBrTags);
            request.Data.SetHtmlFormat(textAsHtml);
        }
    }
}
