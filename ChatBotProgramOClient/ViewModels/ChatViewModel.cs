﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ChatBotProgramOClient.Behavior;
using ChatBotProgramOClient.Models;
using ChatBotProgramOClient.Services;
using Newtonsoft.Json;
using Windows.ApplicationModel.DataTransfer;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.ViewManagement;

namespace ChatBotProgramOClient.ViewModels
{
    public class ChatViewModel : ViewModelBase
    {
        private const string ChatMessagesToken = "ChatMessagesToken";
        private const string ChatInputToken = "ChatInputToken";
        private const string BotIdToken = "BotIdToken";
        private const string ConversationIdToken = "ConversationidToken";
        private readonly List<string> plainTextFileTypes = new List<string>() { ".txt" };
        private readonly IPropertySet localSettingsValues = ApplicationData.Current.LocalSettings.Values;
        private string chatInput;
        private int botId;
        private string conversationId;
        private DataTransferManager dataTransferManager;

        private double chatWidth = 0;
        private double chatHeight = 0;

        public ChatViewModel()
        {
            this.SendMessage = new RelayCommand(this.OnSendMessage);
            this.AppBarHome = new RelayCommand(this.OnAppBarHome);
            this.AppBarClear = new RelayCommand(this.OnAppBarClear);
            this.AppBarExport = new RelayCommand(this.OnAppBarExport);
            this.AppBarImport = new RelayCommand(this.OnAppBarImport);
        }

        public string ChatInput
        {
            get
            {
                return this.chatInput;
            }
            set
            {
                this.SetProperty(ref chatInput, value);
            }
        }

        public ObservableCollection<ChatMessage> ChatMessages { get; set; }

        public RelayCommand SendMessage { get; private set; }

        public RelayCommand AppBarHome { get; private set; }

        public RelayCommand AppBarClear { get; private set; }

        public RelayCommand AppBarExport { get; private set; }

        public RelayCommand AppBarImport { get; private set; }

        public string TextForPrinting
        {
            get
            {
                return this.GenerateTextFromHistoryAsync().Result;
            }
        }

        public string ChatTitle
        {
            get
            {
                return string.Format("Chat with {0}", (BotType)this.botId);
            }
        }

        public double ChatWidth
        {
            get
            {
                if (this.chatWidth == 0)
                {
                    this.chatWidth = Window.Current.Bounds.Width - 480;
                }

                return this.chatWidth;
            }
        }

        public double ChatHeight
        {
            get
            {
                if (this.chatHeight == 0)
                {
                    this.chatHeight = Window.Current.Bounds.Height - 340;
                }

                return this.chatHeight;
            }
        }

        public override void LoadState(object navParameter, Dictionary<string, object> viewModelState)
        {
            this.dataTransferManager = DataTransferManager.GetForCurrentView();
            this.dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
            this.botId = (int)navParameter;

            if (viewModelState != null)
            {
                this.ChatMessages = (ObservableCollection<ChatMessage>)viewModelState[ChatMessagesToken];
                this.ChatInput = (string)viewModelState[ChatInputToken];
            }
            else
            {
                if (this.localSettingsValues.ContainsKey(string.Format("{0}{1}", BotIdToken, this.botId)))
                {
                    this.conversationId = (string)this.localSettingsValues[string.Format("{0}{1}", BotIdToken, this.botId)];
                    try
                    {
                        this.ChatMessages = JsonConvert.DeserializeObject<ObservableCollection<ChatMessage>>(
                            (string)this.localSettingsValues[string.Format("{0}{1}", ConversationIdToken, this.conversationId)]);
                    }
                    catch (Exception)
                    {
                        this.InitializeChat();
                    }
                }
                else
                {
                    this.InitializeChat();
                }
            }
        }

        public override void SaveState(Dictionary<string, object> viewModelState)
        {
            this.dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
            if (this.localSettingsValues.ContainsKey(string.Format("{0}{1}", ConversationIdToken, this.conversationId)))
            {
                viewModelState[ChatMessagesToken] = this.ChatMessages;
                viewModelState[ChatInputToken] = this.ChatInput;
            }
        }

        private async void OnSendMessage(object parameter)
        {
            if (!string.IsNullOrEmpty(this.ChatInput))
            {
                var chatMessage = new ChatMessage()
                {
                    Message = this.ChatInput,
                    Owner = MessageOwner.User
                };
                this.ChatInput = string.Empty;

                this.ChatMessages.Add(chatMessage);
                var messagesListView = parameter as ListView;
                this.ScrollListViewToBottom(messagesListView);

                bool hasConnectionProblems = false;
                try
                {
                    var responseChatDialog = await BotServices.GetBotAnswer(this.botId, this.conversationId, chatMessage.Message);
                    ChatMessage responseChatMessage = new ChatMessage()
                    {
                        Message = responseChatDialog.BotMessage,
                        Owner = MessageOwner.Bot
                    };
                    this.ChatMessages.Add(responseChatMessage);
                    this.ScrollListViewToBottom(messagesListView);
                    this.localSettingsValues[string.Format("{0}{1}", ConversationIdToken, this.conversationId)] = JsonConvert.SerializeObject(this.ChatMessages);
                }
                catch (Exception)
                {
                    hasConnectionProblems = true;
                }

                if (hasConnectionProblems)
                {
                    var messageDialog = new MessageDialog("Please check your internet connection.", "Something went wrong.");
                    messageDialog.Commands.Add(new UICommand("Ok"));
                    await messageDialog.ShowAsync();
                }
            }
        }

        private void OnAppBarHome(object parameter)
        {
            this.NavigationService.GoHome();
        }

        private async void OnAppBarClear(object parameter)
        {
            var messageDialog = new MessageDialog("You are about to permanently delete the current bot chat history.", "Are you sure?");
            messageDialog.Commands.Add(new UICommand(
                "Ok",
                new UICommandInvokedHandler(this.OnAppBarClearConfirmation)));
            messageDialog.Commands.Add(new UICommand("Cancel"));
            messageDialog.DefaultCommandIndex = 0;
            messageDialog.CancelCommandIndex = 1;
            await messageDialog.ShowAsync();
        }

        private void OnAppBarClearConfirmation(IUICommand command)
        {
            this.InitializeChat();
            this.RaiseToastNotification("Chat history cleared successfully!");
        }

        private async void OnAppBarExport(object parameter)
        {
            bool ready = true;
            if (ApplicationView.Value == ApplicationViewState.Snapped)
            {
                ready = ApplicationView.TryUnsnap();
            }

            if (ready)
            {
                var savePicker = new FileSavePicker();
                savePicker.FileTypeChoices.Add("Plain Text", this.plainTextFileTypes);
                savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                savePicker.SuggestedFileName = "ChatBot History.txt";
                var saveFile = await savePicker.PickSaveFileAsync();
                if (saveFile != null)
                {
                    string textContent = await this.GenerateTextFromHistoryAsync();
                    await Windows.Storage.FileIO.WriteTextAsync(saveFile, textContent);
                    this.RaiseToastNotification("Chat history saved successfully!");
                }
            }
        }

        private Task<string> GenerateTextFromHistoryAsync()
        {
            return Task.Run(() =>
            {
                StringBuilder textBuilder = new StringBuilder();
                textBuilder.AppendFormat("ChatBot Dialog History {0}", this.conversationId);
                textBuilder.AppendLine();
                textBuilder.AppendFormat("Generated at {0}", DateTime.Now);
                textBuilder.AppendLine(Environment.NewLine);

                foreach (var chatMessage in this.ChatMessages)
                {
                    switch (chatMessage.Owner)
                    {
                        case MessageOwner.User:
                            textBuilder.Append("You: ");
                            break;
                        case MessageOwner.Bot:
                            textBuilder.Append("Bot: ");
                            break;
                    }

                    textBuilder.AppendLine(chatMessage.Message);
                }

                return textBuilder.ToString();
            });
        }

        private async void OnAppBarImport(object parameter)
        {
            bool ready = true;
            if (ApplicationView.Value == ApplicationViewState.Snapped)
            {
                ready = ApplicationView.TryUnsnap();
            }

            if (ready)
            {
                var messageDialog = new MessageDialog("If you select a file you will permanently replace the current bot chat history.");
                messageDialog.Commands.Add(new UICommand("Ok"));
                messageDialog.DefaultCommandIndex = 0;
                messageDialog.CancelCommandIndex = 0;
                await messageDialog.ShowAsync();

                var openPicker = new FileOpenPicker();
                openPicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                openPicker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
                foreach (var fileType in this.plainTextFileTypes)
                {
                    openPicker.FileTypeFilter.Add(fileType);
                }

                var file = await openPicker.PickSingleFileAsync();
                if (file != null)
                {
                    this.GenerateHistoryFromFileAsync(file);
                }
            }
        }

        private async void GenerateHistoryFromFileAsync(StorageFile file)
        {
            using (var winRtStream = await file.OpenAsync(FileAccessMode.Read))
            {
                Stream dotnetStream = winRtStream.AsStreamForRead();
                using (var streamReader = new StreamReader(dotnetStream))
                {
                    bool isCorruptedFile = false;
                    try
                    {
                        this.ChatMessages.Clear();
                        this.localSettingsValues.Remove(string.Format("{0}{1}", ConversationIdToken, this.conversationId));
                        string line = streamReader.ReadLine();
                        int conversationIdIndex = line.LastIndexOf(' ') + 1;
                        this.conversationId = line.Substring(conversationIdIndex);
                        line = streamReader.ReadLine();
                        line = streamReader.ReadLine();
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            ChatMessage chatMessage = new ChatMessage();
                            string messageSender = line.Substring(0, 3);
                            switch (messageSender)
                            {
                                case "You":
                                    chatMessage.Owner = MessageOwner.User;
                                    break;
                                case "Bot":
                                    chatMessage.Owner = MessageOwner.Bot;
                                    break;
                            }

                            chatMessage.Message = line.Substring(5);
                            this.ChatMessages.Add(chatMessage);
                        }

                        this.localSettingsValues[string.Format("{0}{1}", BotIdToken, this.botId)] = this.conversationId;
                        this.localSettingsValues[string.Format("{0}{1}", ConversationIdToken, this.conversationId)] = JsonConvert.SerializeObject(this.ChatMessages);
                        this.RaiseToastNotification("Chat history loaded successfully!");
                    }
                    catch (Exception)
                    {
                        isCorruptedFile = true;
                        this.localSettingsValues.Remove(string.Format("{0}{1}", BotIdToken, this.botId));
                        this.conversationId = DateTime.Now.Ticks.ToString();
                    }

                    if (isCorruptedFile)
                    {
                        var messageDialog = new MessageDialog("Your file is either not a history file or is corrupted.", "Something went wrong.");
                        messageDialog.Commands.Add(new UICommand("Ok"));
                        messageDialog.DefaultCommandIndex = 0;
                        messageDialog.CancelCommandIndex = 0;
                        await messageDialog.ShowAsync();
                    }
                }
            }
        }

        private void RaiseToastNotification(
            string text,
            bool isSilent = true,
            string imagePath = "ms-appx:///Assets/Logo.scale-100.png")
        {
            string toastXmlString =
                string.Format("<toast><visual version='1'><binding template='toastImageAndText01'><text id='1'>{0}</text><image id='1' src='{1}' alt='BotChat Logo'/></binding></visual><audio silent='{2}'/></toast>", text, imagePath, isSilent.ToString().ToLower());
            XmlDocument toastDom = new XmlDocument();
            toastDom.LoadXml(toastXmlString);
            ToastNotification toast = new ToastNotification(toastDom);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void ScrollListViewToBottom(ListView messagesListView)
        {
            var selectedIndex = messagesListView.Items.Count - 1;
            if (selectedIndex >= 0)
            {
                messagesListView.SelectedIndex = selectedIndex;
                messagesListView.UpdateLayout();

                messagesListView.ScrollIntoView(messagesListView.SelectedItem);
            }
        }

        private void InitializeChat()
        {
            if (this.ChatMessages == null)
            {
                this.ChatMessages = new ObservableCollection<ChatMessage>();
            }
            else
            {
                this.ChatMessages.Clear();
            }

            this.conversationId = DateTime.Now.Ticks.ToString();
            this.localSettingsValues[string.Format("{0}{1}", BotIdToken, this.botId)] = this.conversationId;
            this.localSettingsValues[string.Format("{0}{1}", ConversationIdToken, this.conversationId)] = JsonConvert.SerializeObject(this.ChatMessages);
        }

        private async void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            DataRequest request = args.Request;
            request.Data.Properties.ApplicationName = "ChatBot";
            request.Data.Properties.Title = string.Format("ChatBot {0} History", (BotType)this.botId);
            request.Data.Properties.Description = "ChatBot is a simple AI chat application developed just as a fun project.";
            string textToShare = await this.GenerateTextFromHistoryAsync();
            string textWithBrTags = textToShare.Replace(Environment.NewLine, "<br />");
            string textAsHtml = HtmlFormatHelper.CreateHtmlFormat(textWithBrTags);
            request.Data.SetHtmlFormat(textAsHtml);
        }
    }
}