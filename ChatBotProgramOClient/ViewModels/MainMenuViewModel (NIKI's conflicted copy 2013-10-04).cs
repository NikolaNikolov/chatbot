﻿using System.Collections.Generic;
using ChatBotProgramOClient.Behavior;
using ChatBotProgramOClient.Models;
using Windows.UI.Xaml;

namespace ChatBotProgramOClient.ViewModels
{
    public class MainMenuViewModel : ViewModelBase
    {
        private const double TitleHeight = 140;
        private const double LeftSpaceWidth = 120;

        public MainMenuViewModel()
        {
            this.SelectionChanged = new RelayCommand(this.OnSelectionChanged);
            this.PopulateMainMenuItems();
        }

        public List<MainMenuItem> MainMenuItems { get; set; }

        public RelayCommand SelectionChanged { get; private set; }

        public double MenuItemWidth
        {
            get
            {
                double windowWidth = Window.Current.Bounds.Width;
                double menuItemWidth = (windowWidth - LeftSpaceWidth) / this.MainMenuItems.Count;
                return menuItemWidth - 20;
            }
        }

        public double MenuItemHeight
        {
            get
            {
                return Window.Current.Bounds.Width - TitleHeight;
            }
        }

        private void OnSelectionChanged(object parameter)
        {
            var selectedItem = parameter as MainMenuItem;
            this.NavigationService.Navigate("ChatView", (int)selectedItem.BotType);
        }

        private void PopulateMainMenuItems()
        {
            this.MainMenuItems = new List<MainMenuItem>()
            {
                new MainMenuItem()
                {
                    ImageUrl = "../Assets/CarlosChowBot220x220.png",
                    Title = "Chat with Carlos Chow",
                    Description = "Considered the fun bot. He tries to pull off some one-liners (most of the time unsuccessfully).",
                    BotType = BotType.CarlosChow
                },
                new MainMenuItem()
                {
                    ImageUrl = "../Assets/ShakespeareBot220x220.png",
                    Title = "Chat with Shakespeare Bot",
                    Description = "He thinks he is the real \"Bard of Avon\" and you can't prove him wrong. ",
                    BotType = BotType.ShakespeareBot
                },
                new MainMenuItem()
                {
                    ImageUrl = "../Assets/ProgramOBot220x220.png",
                    Title = "Chat with Program O",
                    Description = "The original chat bot. Can be a bit boring at times, but is most suited for serious conversations.",
                    BotType = BotType.ProgramO
                }
            };
        }
    }
}